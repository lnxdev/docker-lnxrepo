# Docker - Installation, configuration, application examples.

## CentOS 7

### Docker and Cockpit installation with container support

```bash
sudo yum update -y              # Reboot if required
sudo yum remove docker docker-client docker-client-latest docker-common docker-latest \
                docker-latest-logrotate docker-logrotate docker-engine

sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io
sudo systemctl enable --now docker.service && systemctl status docker.service

### ===   Verify the docker installation   === ###
sudo docker run hello-world

### ===   Cockpit installation   === ###
sudo yum install -y cockpit cockpit-docker cockpit-storaged cockpit-system
sudo systemctl enable --now cockpit.service && systemctl status cockpit.service
sudo firewall-cmd --permanent --zone=public --add-service=cockpit
sudo firewall-cmd --reload
```

## CentOS 8

### Docker installation

```bash
sudo dnf update -y            # Reboot if required
sudo dnf remove docker docker-client docker-client-latest docker-common docker-latest \
                docker-latest-logrotate docker-logrotate docker-engine

sudo dnf install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf install -y docker-ce docker-ce-cli containerd.io
sudo systemctl enable --now docker.service && systemctl status docker.service

### ===   Verify the docker installation   === ###
sudo docker run hello-world
```

## Fedora 33

### Docker installation

```bash
sudo dnf update -y            # Reboot if required
sudo dnf remove docker docker-client docker-client-latest docker-common docker-latest \
                docker-latest-logrotate docker-logrotate docker-selinux docker-engine-selinux \
                docker-engine

sudo dnf install -y dnf-plugins-core
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install -y docker-ce docker-ce-cli containerd.io
sudo systemctl enable --now docker.service && systemctl status docker.service

### ===   Verify the docker installation   === ###
sudo docker run hello-world
```

## Ubuntu 20.04

### Docker and docker-compose installation

```bash
sudo apt update; sudo apt upgrade -y            # Reboot if required
sudo apt install docker.io containerd docker-compose -y
sudo systemctl enable --now docker.service && systemctl status docker.service

### ===   Verify the docker installation   === ###
sudo docker run hello-world
```

## Docker with non-root user

If you would like to use Docker as a non-root user, you should consider adding your user to the *docker* group:

```bash
sudo usermod -aG docker your-user
```

**Note**:
> Log out and back in for this to take effect. Adding a user to the *docker* group grants them the ability to run containers which can be used to obtain root privileges on the Docker host.

## Application examples

| Application | Test platform |
|-------------|---------------|
| [Gitlab from dockerfile](app/ubuntu-2004/gitlab/README.md) | Ubuntu 20.04 |
| [OnlyOffice Workspace Community Edition on Ubuntu](app/ubuntu-2004/onlyoffice/README.md) | Ubuntu 20.04 |
| [OpenNMS Network Monitoring Solution on Ubuntu 20.04](https://jsnotes.gitlab.io/article/opennms-network-monitoring-solution-on-ubuntu-2004/) | Ubuntu 20.04 |