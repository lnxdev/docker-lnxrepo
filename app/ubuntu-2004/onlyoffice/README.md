# OnlyOffice Workspace Community Edition on Ubuntu

> My test platform: Ubuntu Server 20.04

## Prerequisites

- CPU: dual core 2 GHz or better
- RAM: 6 GB or more
- HDD: at least 40 GB of free space
- At least 6 GB of swap
- root privileges or sudo privileges

```bash
wget https://download.onlyoffice.com/install/workspace-install.sh

### ===   OnlyOffice with OnlyOffice Mail   === ###
### ===   yourdomain.com is your own domain that will be used for ONLYOFFICE Mail   === ###
bash workspace-install.sh -md "yourdomain.com"

### ===   OnlyOffice without OnlyOffice Mail   === ###
bash workspace-install.sh -ims false

### ===   The list of all available script parameters   === ###
bash workspace-install.sh -h
```