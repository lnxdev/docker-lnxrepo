# Gitlab from dockerfile

> My test platform: Ubuntu Server 20.04

## Prerequisites

- RAM: 4 GB or more
- root privileges or *sudo* privileges

### Set up a project directory and create a docker-compose file.

```shell
mkdir -p projects/gitlab; cd projects/gitlab/
sudo mkdir -p /srv/gitlab/{config/ssl,logs,data}
echo 'GITLAB_HOME=/srv/gitlab' >> .env
```

[Docker-Compose file - Example](docker-compose.yml)

### SSL Letsencrypt for Gitlab installation 

```shell
sudo apt install certbot -y
certbot certonly --rsa-key-size 2048 --standalone --agree-tos --no-eff-email --email sysadmin@lnxdev.lan -d gitlab.lnxdev.lan
cp /etc/letsencrypt/live/gitlab.lnxdev.lan/fullchain.pem /srv/gitlab/config/ssl/
cp /etc/letsencrypt/live/gitlab.lnxdev.lan/privkey.pem /srv/gitlab/config/ssl/
sudo openssl dhparam -out /srv/gitlab/config/ssl/dhparams.pem 2048
sudo tree /srv/gitlab/
```

### Build the Gitlab container

```shell
cd ~/projects/gitlab
sudo docker-compose up -d
sudo docker-compose ps
```

### Check GitLab service status inside the container

```shell
sudo docker exec -it gitlab_web_1 gitlab-ctl status
```

### Access GitLab container

```shell
sudo docker exec -it gitlab_web_1 /bin/bash
cat /etc/lsb-release
```